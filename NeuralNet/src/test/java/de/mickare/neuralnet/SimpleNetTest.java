package de.mickare.neuralnet;

import java.text.DecimalFormat;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;

import junit.framework.TestCase;

import org.jblas.DoubleMatrix;
import org.junit.Test;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.AtomicDouble;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;

import de.mickare.neuralnet.NeuralComputation.TrainingResult;
import de.mickare.neuralnet.function.NeuronFunction;

public class SimpleNetTest extends TestCase {
	
	private static final int POOL_SIZE = Runtime.getRuntime().availableProcessors() * 4;
	
	private static final ExecutorService POOL = MoreExecutors.getExitingExecutorService( new ThreadPoolExecutor(
			POOL_SIZE, POOL_SIZE, 10, TimeUnit.SECONDS, new LinkedBlockingDeque<Runnable>() ), 10, TimeUnit.SECONDS );
	
	public static final boolean OUTPUT_testNetworkBuilder = false;
	
	@Test
	public void testSynapseWeights() {
		NeuronLayer prev = new NeuronLayer( 7, 3 );
		NeuronLayer next = new NeuronLayer( 2, 5 );
		SynapseFull syn = prev.connectFull( next );
		
		DoubleMatrix[] weightsForInput = new DoubleMatrix[next.getRows() * next.getColumns()];
		DoubleMatrix[] weightsForOutput = new DoubleMatrix[prev.getRows() * prev.getColumns()];
		
		for ( int indexNext = 0; indexNext < weightsForInput.length; indexNext++ ) {
			weightsForInput[indexNext] = syn.getInputWeights( indexNext );
		}
		
		for ( int indexPrev = 0; indexPrev < weightsForOutput.length; indexPrev++ ) {
			weightsForOutput[indexPrev] = syn.getOutputWeights( indexPrev );
		}
		
		for ( int indexPrev = 0; indexPrev < weightsForInput.length; indexPrev++ ) {
			final DoubleMatrix outputWeight = new DoubleMatrix( next.getRows(), next.getColumns() );
			for ( int indexNext = 0; indexNext < weightsForInput.length; indexNext++ ) {
				outputWeight.put( indexNext, weightsForInput[indexNext].get( indexPrev ) );
			}
			assertEquals( weightsForOutput[indexPrev], outputWeight );
		}
		
	}
	
	private void assertEquals( DoubleMatrix a, DoubleMatrix b ) {
		a.assertSameSize( b );
		for ( int index = 0; index < a.length; index++ ) {
			assertEquals( a.data[index], b.data[index] );
		}
	}
	
	@Test
	public void testNetworkBuilder() throws InterruptedException, ExecutionException, TimeoutException {
		long start = System.currentTimeMillis();
		NeuralNetwork.Builder builder = NeuralNetwork.builder( 3, 3 );
		NeuronLayer middleA = new NeuronLayer( 10, 10 );
		NeuronLayer middleB = new NeuronLayer( 4, 4 );
		NeuronLayer end = new NeuronLayer( 2, 2 );
		builder.getStartLayer().connectFull( middleA );
		builder.getStartLayer().connectFull( middleB );
		middleA.connectFull( end );
		middleB.connectFull( end );
		NeuralNetwork net = builder.build();
		assertEquals( net.getStartLayer(), builder.getStartLayer() );
		assertEquals( net.getEndLayer(), end );
		
		if ( OUTPUT_testNetworkBuilder )
			System.out.println( "Build network: " + ( System.currentTimeMillis() - start ) + " ms" );
		
	}
	
	public static final boolean OUTPUT_testFeedForward = false;
	
	@Test
	public void testFeedForward() throws InterruptedException, ExecutionException, TimeoutException {
		NeuralNetwork.Builder builder = NeuralNetwork.builder( 3, 3 );
		builder.defaultExecutor( POOL );
		NeuronLayer middleA = new NeuronLayer( 100, 100 );
		NeuronLayer middleB = new NeuronLayer( 100, 100 );
		NeuronLayer end = new NeuronLayer( 2, 2 );
		builder.getStartLayer().connectFull( middleA );
		builder.getStartLayer().connectFull( middleB );
		middleA.connectFull( end );
		middleB.connectFull( end );
		NeuralNetwork net = builder.build();
		
		ListenableFuture<DoubleMatrix> f;
		
		final DoubleMatrix randomInput = DoubleMatrix.rand( 3, 3 );
		
		if ( OUTPUT_testFeedForward ) {
			System.out.println( "Neurons: " + net.getNeuronLayers().size() );
			System.out.println( "Synapses: " + net.getSynapses().size() );
			System.out.println( "Input:" );
			randomInput.print();
			System.out.println( "Output:" );
		}
		
		final int runs = 10;
		
		final List<ListenableFuture<DoubleMatrix>> futures = Lists.newArrayListWithCapacity( runs );
		
		// final List<ListenableFuture<DoubleMatrix>> futures = Lists.newLinkedList();
		
		final int messages = 10;
		final int messagesModulo = ( runs / messages == 0 ) ? 1 : ( runs / messages );
		long start = System.currentTimeMillis();
		for ( int i = 0; i < runs; i++ ) {
			final int number = i;
			f = net.compute( randomInput );
			if ( OUTPUT_testFeedForward )
				Futures.addCallback( f, new FutureCallback<DoubleMatrix>() {
					@Override
					public void onSuccess( DoubleMatrix result ) {
						if ( number % messagesModulo == 0 ) {
							result.print();
						}
					}
					
					@Override
					public void onFailure( Throwable t ) {
					}
				} );
			futures.add( f );
		}
		Futures.successfulAsList( futures ).get( 1, TimeUnit.MINUTES );
		
		if ( OUTPUT_testFeedForward )
			System.out.println( "Time for " + runs + " runs: " + ( System.currentTimeMillis() - start ) + " ms" );
	}
	
	private NeuralNetwork a() {
		NeuralNetwork.Builder builder = NeuralNetwork.builder( 2, 1 );
		builder.defaultExecutor( POOL );
		NeuronLayer A = new NeuronLayer( 2, 3, NeuronFunction.TANH );
		NeuronLayer B = new NeuronLayer( 5, 1, NeuronFunction.TANH );
		NeuronLayer middle = new NeuronLayer( 1, 4, NeuronFunction.TANH );
		NeuronLayer end = new NeuronLayer( 1, 1, NeuronFunction.TANH );
		builder.getStartLayer().connectFull( A );
		A.connectFull( B );
		B.connectFull( middle );
		middle.connectFull( end );
		return builder.build();
	}
	
	private NeuralNetwork b() {
		NeuralNetwork.Builder builder = NeuralNetwork.builder( 2, 1 );
		builder.defaultExecutor( POOL );
		NeuronLayer middle = new NeuronLayer( 1, 4, NeuronFunction.TANH );
		NeuronLayer end = new NeuronLayer( 1, 1, NeuronFunction.TANH );
		builder.getStartLayer().connectFull( middle );
		middle.connectFull( end );
		return builder.build();
	}
	
	@Test
	public void testFeedBackward() throws InterruptedException, ExecutionException, TimeoutException {
		
		final NeuralNetwork net = b();
		
		net.getTrainingSettings().setLearningRate( t -> Math.exp( -t / 10000 ) / 4 + 0.10 );
		net.getTrainingSettings().setMomentum( t -> Math.exp( -t / 10000 ) / 4 + 0.20 );
		
		final double TRUE = 1;
		final double FALSE = 0;
		
		final List<Map.Entry<DoubleMatrix, DoubleMatrix>> testData = Lists.newArrayListWithExpectedSize( 4 );
		testData.add( new AbstractMap.SimpleImmutableEntry<DoubleMatrix, DoubleMatrix>( new DoubleMatrix( 2, 1, FALSE,
				FALSE ), DoubleMatrix.ones( 1, 1 ).muli( FALSE ) ) );
		testData.add( new AbstractMap.SimpleImmutableEntry<DoubleMatrix, DoubleMatrix>( new DoubleMatrix( 2, 1, FALSE,
				TRUE ), DoubleMatrix.ones( 1, 1 ).muli( TRUE ) ) );
		testData.add( new AbstractMap.SimpleImmutableEntry<DoubleMatrix, DoubleMatrix>( new DoubleMatrix( 2, 1, TRUE,
				FALSE ), DoubleMatrix.ones( 1, 1 ).muli( TRUE ) ) );
		testData.add( new AbstractMap.SimpleImmutableEntry<DoubleMatrix, DoubleMatrix>( new DoubleMatrix( 2, 1, TRUE,
				TRUE ), DoubleMatrix.ones( 1, 1 ).muli( FALSE ) ) );
		
		final AtomicDouble oldError = new AtomicDouble( 0 );
		
		final DecimalFormat df = new DecimalFormat( "0.0000000" );
		
		// ***************************************
		// Training
		final BiFunction<Integer, TrainingResult, Boolean> trainFinishCallback = ( run, result ) -> {
			
			double avgError = net.getAverageError().getAverageError();
			double errorDelta = avgError - oldError.getAndSet( avgError );
			
			System.out.println( Strings.padStart( Integer.toString( run ), 9, ' ' ) + " -> avgErr: "
					+ df.format( net.getAverageError().getAverageError() ) + " | " + ( errorDelta < 0 ? "" : " " )
					+ df.format( errorDelta ) );
			
			if ( net.getAbsolvedTraingingsCount() > 10000 && result.getError() > 0.1 ) {
				System.out.println( "Staaaaaap!" );
				return false;
			} else if ( run % 1000 != 0 ) {
				return true;
			}
			
			// System.out.println( "." );
			// System.out.println( run );
			// System.out.println( "in:  " + result.getInput() );
			// System.out.println( "out: " + result.getOutput() );
			// System.out.println( "exp: " + result.getExpectedOutput() );
			// System.out.println( "err: " + result.getError() + ( result.getError() > 0.01 ? "          <- !!!" : "" )
			// );
			
			// System.out.println( " -> avgErr: " + net.getAverageError().getAverageError() );
			return true;
		};
		train( net, testData, 100000, trainFinishCallback );
		
		// ***************************************
		// Test
		runTest( net, testData, 10000, ( result, expected ) -> {
			boolean a = Math.abs( TRUE - result.get( 0 ) ) < 0.1;
			boolean b = Math.abs( TRUE - expected.get( 0 ) ) < 0.1;
			return ( a && b ) || ( !a && !b );
		} );
		
		// ***************************************
		// Test
		runTest( net, testData, 4, ( result, expected ) -> {
			boolean a = Math.abs( TRUE - result.get( 0 ) ) < 0.1;
			boolean b = Math.abs( TRUE - expected.get( 0 ) ) < 0.1;
			System.out.println( "exp: " + expected + " - out: " + result );
			return ( a && b ) || ( !a && !b );
		} );
		
	}
	
	private void train( final NeuralNetwork network, final List<Map.Entry<DoubleMatrix, DoubleMatrix>> testData,
			final int trainings, final BiFunction<Integer, TrainingResult, Boolean> trainFinishCallback )
			throws InterruptedException, ExecutionException, TimeoutException {
		final long start = System.currentTimeMillis();
		final Semaphore parallels = new Semaphore( 1 );
		final AtomicBoolean stop = new AtomicBoolean( false );
		ListenableFuture<TrainingResult> last = Futures.immediateFuture( null );
		
		for ( int t = 1; t <= trainings && !stop.get(); t++ ) {
			final int run = t;
			
			final Map.Entry<DoubleMatrix, DoubleMatrix> e = testData.get( run % testData.size() );
			final DoubleMatrix input = e.getKey();
			final DoubleMatrix expectedResult = e.getValue();
			
			final ListenableFuture<TrainingResult> f = network.train( input, expectedResult );
			last = f;
			
			// TrainingResult result = f.get( 1, TimeUnit.SECONDS );
			
			Futures.addCallback( f, new FutureCallback<TrainingResult>() {
				@Override
				public void onSuccess( final TrainingResult result ) {
					try {
						synchronized ( stop ) {
							if ( !trainFinishCallback.apply( run, result ) ) {
								stop.set( true );
							}
						}
					} finally {
						parallels.release();
					}
				}
				
				@Override
				public void onFailure( Throwable t ) {
					parallels.release();
				}
			} );
			
			// if ( t > 10000 && net.getAverageError().getAverageError() < 0.1 && result.getError() > 0.01 ) {
			
			// break;
			// } else {
			// printPoint( t, 70, "." );
			// }
			
			parallels.acquire();
			
		}
		last.get( 10, TimeUnit.SECONDS );
		Thread.sleep( 50 );
		
		System.out.println( trainings + " Trainings in " + ( System.currentTimeMillis() - start + " ms completed." ) );
	}
	
	private void runTest( final NeuralNetwork network, final List<Map.Entry<DoubleMatrix, DoubleMatrix>> testData,
			final int runs, BiPredicate<DoubleMatrix, DoubleMatrix> resultEqualsExpectedPredicate )
			throws InterruptedException, ExecutionException, TimeoutException {
		
		final AtomicInteger fails = new AtomicInteger( 0 );
		final long start = System.currentTimeMillis();
		ListenableFuture<DoubleMatrix> last = Futures.immediateFuture( null );
		
		for ( int r = 1; r <= runs; r++ ) {
			final Map.Entry<DoubleMatrix, DoubleMatrix> e = testData.get( r % testData.size() );
			final DoubleMatrix input = e.getKey();
			final DoubleMatrix expectedResult = e.getValue();
			ListenableFuture<DoubleMatrix> f = network.compute( input );
			last = f;
			Futures.addCallback( f, new FutureCallback<DoubleMatrix>() {
				@Override
				public void onSuccess( final DoubleMatrix result ) {
					if ( !resultEqualsExpectedPredicate.test( result, expectedResult ) ) {
						fails.incrementAndGet();
					}
				}
				
				@Override
				public void onFailure( Throwable t ) {
					fails.incrementAndGet();
				}
			} );
		}
		last.get( 10, TimeUnit.SECONDS );
		Thread.sleep( 50 );
		System.out.println( runs + " Calculation runs in "
				+ ( System.currentTimeMillis() - start + " ms completed (" + fails.get() + " failed)." ) );
	}
	
	private void printPoint( int t, int lenght, String s ) {
		if ( t % lenght == 0 ) {
			System.out.println( s );
		} else {
			System.out.print( s );
		}
	}
	
}
