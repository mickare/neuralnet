package de.mickare.neuralnet.training;

import java.util.function.Function;

import com.google.common.base.Preconditions;

import de.mickare.neuralnet.NeuralNetwork;
import de.mickare.neuralnet.util.concurrent.CloseableLock;
import de.mickare.neuralnet.util.concurrent.CloseableReentrantReadWriteLock;

public class NetworkTrainingSettings {
	
	private final NeuralNetwork network;
	
	private final CloseableReentrantReadWriteLock lock = new CloseableReentrantReadWriteLock();
	private Function<Long, Double> learningRate = ( r -> 0.15 ), momentum = ( m -> 0.5 );
	
	public NetworkTrainingSettings( NeuralNetwork network ) {
		Preconditions.checkNotNull( network );
		this.network = network;
	}
	
	// ****************************************************
	// Setter
	
	public void setLearningRate( final double learningRate ) {
		try ( CloseableLock cl = lock.writeLock().open() ) {
			this.learningRate = ( r -> learningRate );
		}
	}
	
	public void setLearningRate( final Function<Long, Double> learningRate ) {
		try ( CloseableLock cl = lock.writeLock().open() ) {
			this.learningRate = learningRate;
		}
	}
	
	public void setMomentum( final double momentum ) {
		try ( CloseableLock cl = lock.writeLock().open() ) {
			this.momentum = ( r -> momentum );
		}
	}
	
	public void setMomentum( final Function<Long, Double> momentum ) {
		try ( CloseableLock cl = lock.writeLock().open() ) {
			this.momentum = momentum;
		}
	}
	
	// ****************************************************
	// Getter
	
	public Function<Long, Double> getLearningRateFunction() {
		try ( CloseableLock cl = lock.readLock().open() ) {
			return this.learningRate;
		}
	}
	
	public double getLearningRate() {
		try ( CloseableLock cl = lock.readLock().open() ) {
			return this.learningRate.apply( network.getAbsolvedTraingingsCount() );
		}
	}
	
	public Function<Long, Double> getMomentumFunction() {
		try ( CloseableLock cl = lock.readLock().open() ) {
			return this.momentum;
		}
	}
	
	public double getMomentum() {
		try ( CloseableLock cl = lock.readLock().open() ) {
			return this.momentum.apply( network.getAbsolvedTraingingsCount() );
		}
	}
	
}
