package de.mickare.neuralnet.training;

import java.util.Optional;

import lombok.Getter;

import com.google.common.base.Preconditions;

import de.mickare.neuralnet.NeuralComputation;

public class LayerTrainingSettings {
	
	@Getter
	private Optional<Double> learningRate = Optional.empty(), momentum = Optional.empty();
	
	public synchronized void setLearningRate( final double learningRate ) {
		Preconditions.checkArgument( 0d <= learningRate && learningRate <= 1d, "Wrong learning rate! [0.0,..,1.0]" );
		this.learningRate = Optional.of( learningRate );
	}
	
	public synchronized void setLearningRateToDefault() {
		this.learningRate = Optional.empty();
	}
	
	public synchronized void setMomentum( final double momentum ) {
		Preconditions.checkArgument( 0d <= momentum, "Wrong momentum! [0.0,..,n]" );
		this.momentum = Optional.of( momentum );
	}
	
	public synchronized void setMomentumToDefault() {
		this.momentum = Optional.empty();
	}
	
	public double getLearningRate( final NeuralComputation computation ) {
		return this.learningRate.orElse( computation.getLearningRate() );
	}
	
	public double getMomentum( final NeuralComputation computation ) {
		return this.momentum.orElse( computation.getMomentum() );
	}
	
}
