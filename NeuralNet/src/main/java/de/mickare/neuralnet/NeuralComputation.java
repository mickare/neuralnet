package de.mickare.neuralnet;

import java.util.Arrays;
import java.util.concurrent.ExecutionException;

import lombok.Data;
import lombok.Getter;

import org.jblas.DoubleMatrix;

import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;

public class NeuralComputation {
	
	@Data
	public static class TrainingResult {
		@Getter
		private final double error;
		@Getter
		private final DoubleMatrix input;
		@Getter
		private final DoubleMatrix expectedOutput;
		@Getter
		private final DoubleMatrix output;
		@Getter
		private final long duration;
		@Getter
		private final long trainingID;
	}
	
	@Getter
	private final ListeningExecutorService executor;
	
	@Getter
	private final DoubleMatrix input;
	
	@Getter
	private final NeuralNetwork network;
	
	@Getter
	private final double learningRate, momentum;
	
	private final Supplier<ListenableFuture<DoubleMatrix>> task = Suppliers.memoize( ( ) -> {
		this.network.getNeuronLayers().forEach( layer -> layer.getResult( this ) );
		return this.network.getEndLayer().getResult( this );
	} );
	
	// ************************************************************
	// Constructors
	
	protected NeuralComputation( ListeningExecutorService executor, NeuralNetwork network, DoubleMatrix input ) {
		Preconditions.checkNotNull( executor );
		Preconditions.checkNotNull( network );
		Preconditions.checkState( !executor.isShutdown() );
		Preconditions.checkNotNull( input );
		this.executor = executor;
		this.network = network;
		this.input = input;
		
		learningRate = network.getTrainingSettings().getLearningRate();
		momentum = network.getTrainingSettings().getMomentum();
		
		input.checkRows( network.getStartLayer().getRows() );
		input.checkColumns( network.getStartLayer().getColumns() );
		this.results.put( network.getStartLayer(), Futures.immediateFuture( input ) );
		
	}
	
	// ************************************************************
	// Computation Results
	
	private final LoadingCache<ResultSupplier, ListenableFuture<DoubleMatrix>> results = CacheBuilder.newBuilder()
			.weakKeys().build( new CacheLoader<ResultSupplier, ListenableFuture<DoubleMatrix>>() {
				@Override
				public ListenableFuture<DoubleMatrix> load( final ResultSupplier calc ) throws Exception {
					return calc.calculateResult( NeuralComputation.this );
				}
			} );
	
	public final ListenableFuture<DoubleMatrix> getResult( final ResultSupplier calc ) {
		Preconditions.checkNotNull( calc );
		return this.results.getUnchecked( calc );
	}
	
	// ************************************************************
	// Computation Gradients
	
	private final LoadingCache<GradientErrorSupplier, DoubleMatrix> gradientErrors = CacheBuilder.newBuilder()
			.weakKeys().build( new CacheLoader<GradientErrorSupplier, DoubleMatrix>() {
				@Override
				public DoubleMatrix load( final GradientErrorSupplier calc ) throws Exception {
					return calc.calculateHiddenGradientError( NeuralComputation.this );
				}
			} );
	
	public final DoubleMatrix getGradientError( final GradientErrorSupplier calc ) {
		Preconditions.checkNotNull( calc );
		return this.gradientErrors.getUnchecked( calc );
	}
	
	// ************************************************************
	// FeedFordward
	
	public ListenableFuture<DoubleMatrix> feedForward() {
		return task.get();
	}
	
	// ************************************************************
	// Back-Propagation / Training
	
	public ListenableFuture<TrainingResult> train( final DoubleMatrix expectedOutput ) {
		return Futures.transform( feedForward(), new com.google.common.base.Function<DoubleMatrix, TrainingResult>() {
			@Override
			public TrainingResult apply( final DoubleMatrix output ) {
				try {
					return NeuralComputation.this.network.doTraining( NeuralComputation.this, expectedOutput, output );
				} catch ( InterruptedException | ExecutionException e ) {
					throw new RuntimeException( e );
				}
			}
		} );
	}
	
	public TrainingResult doTraining( DoubleMatrix expectedOutput, DoubleMatrix output ) throws InterruptedException,
			ExecutionException {
		return this._doTraining( expectedOutput, output );
	}
	
	private TrainingResult _doTraining( final DoubleMatrix expectedOutput, final DoubleMatrix output )
			throws InterruptedException, ExecutionException {
		
		final long start = System.currentTimeMillis();
		
		final DoubleMatrix delta = expectedOutput.sub( output );
		
		final double overallError = this.overallError( delta );
		
		// -----------------------------------------
		// Output Layer uses the output for error!
		// -----------------------------------------
		// delta = target - output;
		// error = delta * f'(output);
		final DoubleMatrix gradientErrorOutput = delta.mul( MatrixUtils.apply( NeuralComputation.this.network
				.getEndLayer().getFunction().derivate(), output.dup() ) );
		gradientErrors.put( NeuralComputation.this.network.getEndLayer(), gradientErrorOutput );
		
		// Calculate gradient errors
		for ( NeuronLayer layer : this.network.getNeuronLayers().descendingSet() ) {
			layer.getGradientError( this );
		}
		
		// Update weights
		for ( NeuronLayer layer : this.network.getNeuronLayers().descendingSet() ) {
			layer.updateWeights( this );
		}
		
		this.network.getAverageError().addError( overallError );
		return new TrainingResult( overallError, this.input, expectedOutput, output,
				start - System.currentTimeMillis(), this.network.didAbsolveTraining() );
	}
	
	private final double overallError( final DoubleMatrix delta ) {
		return Math.sqrt( Arrays.stream( delta.mul( delta ).data ).sum() / ( delta.length ) );
	}
	
	/*
	 * public final <V> ListenableFuture<V> submit( final Callable<V> task ) { return this.executor.submit( task ); }
	 */
	
}
