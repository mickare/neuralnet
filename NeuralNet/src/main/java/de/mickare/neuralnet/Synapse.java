package de.mickare.neuralnet;

import java.util.function.BiFunction;

import lombok.Getter;

import org.jblas.DoubleMatrix;

import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.ListenableFuture;

public abstract class Synapse implements Comparable<Synapse>, ResultSupplier, GradientErrorSupplier {
	
	public interface Connector<S extends Synapse> extends BiFunction<NeuronLayer, NeuronLayer, S> {
		S apply( NeuronLayer from, NeuronLayer to );
	}
	
	@Getter
	private final NeuronLayer prev, next;
	
	// ************************************************************
	// Constructors
	
	public Synapse( NeuronLayer from, NeuronLayer to ) {
		Preconditions.checkNotNull( from );
		Preconditions.checkNotNull( to );
		Preconditions.checkArgument( from != to, "no loop" );
		this.prev = from;
		this.next = to;
	}
	
	public ListenableFuture<DoubleMatrix> getResult( final NeuralComputation computation ) {
		return computation.getResult( this );
	}
	
	public final DoubleMatrix getGradientError( NeuralComputation computation ) {
		return computation.getGradientError( this );
	}
	
	// ************************************************************
	// Weights updater
	
	protected abstract void updateWeights( final NeuralComputation computation,
			final DoubleMatrix gradientErrorOfNextLayerNeurons );
	
	/*
	 * protected abstract void updateWeights( final NeuronComputation computation, final DoubleMatrix
	 * errorGradientsOfThisSynapse );
	 */
	
	@Override
	public int compareTo( Synapse o ) {
		if ( this == o ) {
			return 0;
		}
		int comp = this.prev.compareTo( o.prev );
		if ( comp != 0 ) {
			return comp;
		}
		comp = this.next.compareTo( o.next );
		if ( comp != 0 ) {
			return comp;
		}
		return Integer.compare( this.hashCode(), o.hashCode() );
	}
	
	public abstract int countNeuronSynapses();
	
}
