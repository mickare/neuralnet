package de.mickare.neuralnet.util;

import java.util.Collection;
import java.util.Set;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

public class EdgeUtils {
	
	private EdgeUtils() {
	}
	
	public static final <V> boolean reduceStep( final V start, final Multimap<V, V> edges )
			throws IllegalStateException {
		Preconditions.checkNotNull( edges );
		
		boolean changed = false;
		
		final Set<V> childs = Sets.newHashSet();
		for ( final V child : edges.removeAll( start ) ) {
			if ( child == start ) {
				throw new IllegalStateException( "loop" );
			}
			final Collection<V> grandChilds = edges.containsValue( child ) ? edges.get( child ) : edges
					.removeAll( child );
			if ( grandChilds.isEmpty() ) {
				// No further grandchilds, so there is nothing to reduce
				childs.add( child );
			} else {
				if ( grandChilds.contains( start ) ) {
					throw new IllegalStateException( "loop" );
				}
				
				// Add grandchilds to parent
				childs.addAll( grandChilds );
				changed = true;
			}
		}
		
		// Add grandchilds
		edges.putAll( start, childs );
		return changed;
	}
	
	public static final <V> boolean reduceSingle( final V element, final Multimap<V, V> edges )
			throws IllegalStateException {
		Preconditions.checkNotNull( edges );
				
		boolean changed = false;
		while ( reduceStep( element, edges ) ) {
			changed = true;
		}		
		return changed;
	}
	
	public static final <V> void reduceAll( final V start, final Multimap<V, V> edges ) throws IllegalStateException {
		reduceSingle( start, edges );
		reduceAll( edges );
	}
	
	public static final <V> void reduceAll( final Multimap<V, V> edges ) throws IllegalStateException {
		Preconditions.checkNotNull( edges );
		
		outer: while ( true ) {
			
			for ( final V element : ImmutableSet.copyOf( edges.keySet() ) ) {
				if ( reduceSingle( element, edges ) ) {
					continue outer;
				}
			}
			break;
		}
		
	}
}
