package de.mickare.neuralnet.util.concurrent;

import java.util.concurrent.locks.ReadWriteLock;

public interface CloseableReadWriteLock extends ReadWriteLock {

	public CloseableLock readLock();

	public CloseableLock writeLock();

}
