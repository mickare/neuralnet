package de.mickare.neuralnet.util;

import com.google.common.base.Preconditions;

import lombok.Getter;

public class SimpleAverageError implements AverageError {
	
	@Getter
	private volatile double averageError = 0d;
	@Getter
	private volatile double averageErrorSmoothingFactor = 0d;
	
	public SimpleAverageError() {
		
	}
	
	public SimpleAverageError( double averageErrorSmoothingFactor ) {
		this.averageErrorSmoothingFactor = averageErrorSmoothingFactor;
	}
	
	public synchronized final void setAverageErrorSmoothingFactor( final double factor ) {
		Preconditions.checkArgument( factor > -1 );
		this.averageErrorSmoothingFactor = factor;
	}
	
	@Override
	public synchronized double addError( double error ) {
		this.averageError = ( this.averageError * this.averageErrorSmoothingFactor + error )
				/ ( this.averageErrorSmoothingFactor + 1 );
		return this.averageError;
	}
	
}
