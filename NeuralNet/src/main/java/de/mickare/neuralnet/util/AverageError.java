package de.mickare.neuralnet.util;

public interface AverageError {
	
	double getAverageError();
	
	double addError( final double error );
	
}
