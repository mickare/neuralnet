package de.mickare.neuralnet.util.concurrent;

import java.util.concurrent.locks.Lock;

public interface CloseableLock extends Lock, AutoCloseable {

	public CloseableLock open();
	
	public void close();
	
}
