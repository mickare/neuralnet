package de.mickare.neuralnet;

import java.util.function.Function;

import org.jblas.DoubleMatrix;

import com.google.common.base.Preconditions;

public class MatrixUtils {
	
	private MatrixUtils() {
	}
	
	public static DoubleMatrix apply( Function<Double, Double> function, DoubleMatrix matrix ) {
		for ( int index = 0; index < matrix.data.length; index++ ) {
			matrix.data[index] = function.apply( matrix.data[index] );
		}
		return matrix;
	}
	
	public final static DoubleMatrix checkForNaN( final DoubleMatrix matrix ) {
		for ( int index = 0; index < matrix.length; index++ ) {
			Preconditions.checkArgument( !Double.isNaN( matrix.data[index] ) );
		}
		return matrix;
	}
}
