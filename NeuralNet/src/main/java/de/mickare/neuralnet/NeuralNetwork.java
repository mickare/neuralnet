package de.mickare.neuralnet;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicLong;

import lombok.Getter;

import org.jblas.DoubleMatrix;

import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import de.mickare.neuralnet.NeuralComputation.TrainingResult;
import de.mickare.neuralnet.function.NeuronFunction;
import de.mickare.neuralnet.training.NetworkTrainingSettings;
import de.mickare.neuralnet.util.AverageError;
import de.mickare.neuralnet.util.EdgeUtils;
import de.mickare.neuralnet.util.SimpleAverageError;

public class NeuralNetwork {
	
	public static Builder builder( int rows, int columns ) {
		return new Builder( rows, columns );
	}
	
	public static Builder builder( final NeuronLayer startLayer ) {
		return new Builder( startLayer );
	}
	
	public static class Builder {
		
		private Optional<ListeningExecutorService> defaultExecutor = Optional.empty();
		
		@Getter
		private final NeuronLayer startLayer;
		
		public Builder( int rows, int columns ) {
			this( new NeuronLayer( rows, columns, NeuronFunction.TANH ) );
		}
		
		public Builder( final NeuronLayer startLayer ) {
			Preconditions.checkNotNull( startLayer );
			Preconditions.checkArgument( !startLayer.isClosed() );
			Preconditions.checkArgument( startLayer.getIncoming().isEmpty() );
			this.startLayer = startLayer;
		}
		
		public Builder defaultExecutor( ExecutorService defaultExecutor ) {
			return this.defaultExecutor( MoreExecutors.listeningDecorator( defaultExecutor ) );
		}
		
		public Builder defaultExecutor( ListeningExecutorService defaultExecutor ) {
			Preconditions.checkState( !defaultExecutor.isShutdown() );
			this.defaultExecutor = Optional.of( defaultExecutor );
			return this;
		}
		
		public Multimap<NeuronLayer, Synapse> getAllSynapses() {
			final HashMultimap<NeuronLayer, Synapse> map = HashMultimap.create();
			addSynapsesForward( this.startLayer, Sets.newHashSet(), map );
			addSynapsesBackward( this.startLayer, Sets.newHashSet(), map );
			return map;
		}
		
		private final static void addSynapsesForward( final NeuronLayer layer, final Set<NeuronLayer> visited,
				final Multimap<NeuronLayer, Synapse> map ) {
			if ( !visited.add( layer ) ) {
				return;
			}
			layer.getOutgoing().forEach( syn -> {
				map.put( syn.getPrev(), syn );
				addSynapsesForward( syn.getNext(), visited, map );
			} );
		}
		
		private final static void addSynapsesBackward( final NeuronLayer layer, final Set<NeuronLayer> visited,
				final Multimap<NeuronLayer, Synapse> map ) {
			if ( !visited.add( layer ) ) {
				return;
			}
			layer.getIncoming().forEach( syn -> {
				map.put( syn.getPrev(), syn );
				addSynapsesBackward( syn.getPrev(), visited, map );
			} );
		}
		
		public NeuralNetwork build() {
			Preconditions.checkArgument( startLayer.getIncoming().isEmpty(), "input has incoming synapses" );
			
			final Multimap<NeuronLayer, Synapse> synapses = this.getAllSynapses();
			
			Preconditions.checkState( !synapses.isEmpty(), "no synapses added" );
			
			// Create Edge Multimap and reduce it to main edges
			final Multimap<NeuronLayer, NeuronLayer> edges = HashMultimap
					.create( synapses.size(), synapses.size() / 2 + 1 );
			synapses.values().forEach( syn -> edges.put( syn.getPrev(), syn.getNext() ) );
			EdgeUtils.reduceAll( this.startLayer, edges );
			
			// Check for endpoint
			Preconditions.checkState( !( edges.values().size() == 0 ), "No endpoint" );
			Preconditions.checkState( !( edges.values().size() > 1 ), "Multiple endpoints!" );
			final NeuronLayer endLayer = edges.values().iterator().next();
			
			if ( endLayer == this.startLayer ) {
				throw new IllegalStateException( "Startpoint can't be equal to endpoint!" );
			}
			
			return new NeuralNetwork(
					this.defaultExecutor.orElseGet( ( ) -> MoreExecutors.newDirectExecutorService() ), this.startLayer,
					ImmutableSet.copyOf( synapses.values() ), endLayer );
		}
	}
	
	private final ListeningExecutorService defaultExecutor;
	
	@Getter
	private final NeuronLayer startLayer;
	
	@Getter
	private final ImmutableSet<Synapse> synapses;
	
	@Getter
	private final ImmutableSortedSet<NeuronLayer> neuronLayers;
	
	@Getter
	private final NeuronLayer endLayer;
	
	@Getter
	private final AverageError averageError = new SimpleAverageError( 3.0 );
	
	private final AtomicLong trainingAbsolved = new AtomicLong( 0 );
	
	@Getter
	private final NetworkTrainingSettings trainingSettings = new NetworkTrainingSettings( this );
	
	private NeuralNetwork( final ListeningExecutorService defaultExecutor, final NeuronLayer startLayer,
			final ImmutableSet<Synapse> synapses, final NeuronLayer endLayer ) {
		Preconditions.checkNotNull( defaultExecutor );
		Preconditions.checkState( !defaultExecutor.isShutdown() );
		Preconditions.checkNotNull( startLayer );
		Preconditions.checkNotNull( synapses );
		Preconditions.checkArgument( !synapses.isEmpty() );
		Preconditions.checkNotNull( endLayer );
		Preconditions.checkArgument( startLayer != endLayer );
		this.defaultExecutor = defaultExecutor;
		this.startLayer = startLayer;
		this.synapses = synapses;
		this.endLayer = endLayer;
		
		final ImmutableSortedSet.Builder<NeuronLayer> neuronLayersBuilder = ImmutableSortedSet
				.orderedBy( ( o1, o2 ) -> Integer.compare( o1.distanceBackwardsTo( this.startLayer ), o2
						.distanceBackwardsTo( this.startLayer ) ) );
		for ( final Synapse syn : this.synapses ) {
			neuronLayersBuilder.add( syn.getPrev() );
			neuronLayersBuilder.add( syn.getNext() );
		}
		this.neuronLayers = neuronLayersBuilder.build();
		
		closeNeurons( this.synapses );
	}
	
	// ***************************************************************
	// Getter / Setter
	
	public long getAbsolvedTraingingsCount() {
		return this.trainingAbsolved.get();
	}
	
	public long didAbsolveTraining() {
		return this.trainingAbsolved.incrementAndGet();
	}
	
	// ***************************************************************
	// Helper Functions
	
	private final void closeNeurons( Collection<Synapse> c ) {
		this.startLayer.close();
		for ( Synapse syn : c ) {
			syn.getPrev().close();
			syn.getNext().close();
		}
	}
	
	// ***************************************************************
	// Normal compute
	
	public ListenableFuture<DoubleMatrix> compute( DoubleMatrix input ) {
		return this.compute( input, this.defaultExecutor );
	}
	
	public ListenableFuture<DoubleMatrix> compute( DoubleMatrix input, ExecutorService executor ) {
		Preconditions.checkNotNull( input );
		Preconditions.checkNotNull( executor );
		return this.compute( input, MoreExecutors.listeningDecorator( executor ) );
	}
	
	public ListenableFuture<DoubleMatrix> compute( DoubleMatrix input, ListeningExecutorService executor ) {
		Preconditions.checkNotNull( input );
		Preconditions.checkNotNull( executor );
		Preconditions.checkState( !executor.isShutdown() );
		return new NeuralComputation( executor, this, input ).feedForward();
	}
	
	// ***************************************************************
	// Training
	
	public ListenableFuture<NeuralComputation.TrainingResult> train( DoubleMatrix input, DoubleMatrix expectedOutput ) {
		return this.train( input, expectedOutput, defaultExecutor );
	}
	
	public ListenableFuture<NeuralComputation.TrainingResult> train( DoubleMatrix input, DoubleMatrix expectedOutput,
			ExecutorService executor ) {
		Preconditions.checkNotNull( input );
		Preconditions.checkNotNull( expectedOutput );
		Preconditions.checkNotNull( executor );
		return this.train( input, expectedOutput, MoreExecutors.listeningDecorator( executor ) );
	}
	
	public ListenableFuture<NeuralComputation.TrainingResult> train( DoubleMatrix input, DoubleMatrix expectedOutput,
			ListeningExecutorService executor ) {
		Preconditions.checkNotNull( input );
		Preconditions.checkNotNull( expectedOutput );
		Preconditions.checkNotNull( executor );
		Preconditions.checkState( !executor.isShutdown() );
		return new NeuralComputation( executor, this, input ).train( expectedOutput );
	}
	
	protected synchronized TrainingResult doTraining( NeuralComputation computation, DoubleMatrix expectedOutput,
			DoubleMatrix output ) throws InterruptedException, ExecutionException {
		Preconditions.checkNotNull( computation );
		Preconditions.checkNotNull( expectedOutput );
		Preconditions.checkNotNull( output );
		return computation.doTraining( expectedOutput, output );
	}
	
}
