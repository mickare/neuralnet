package de.mickare.neuralnet.function;

import java.util.function.Function;

public interface NeuronFunction extends Function<Double, Double> {
	
	// public static DerivativableFunction TANH = new ForwardingDerivativableFunction( Math::tanh, x -> 1d - x * x );
	
	public static NeuronFunction TANH = new ForwardingNeuronFunction( Math::tanh, x -> 1d - x * x );
	
	public static NeuronFunction SIGMOID = new ForwardingNeuronFunction( x -> 1 / ( 1 + Math.exp( -x ) ), x -> {
		final double f = 1 / ( 1 + Math.exp( -x ) );
		return f * ( 1 - f );
	} );
	
	public Function<Double, Double> derivate();
	
}
