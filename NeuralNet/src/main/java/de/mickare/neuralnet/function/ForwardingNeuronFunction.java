package de.mickare.neuralnet.function;

import java.util.function.Function;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public final class ForwardingNeuronFunction implements NeuronFunction {
	
	@NonNull
	private final Function<Double, Double> function;
	
	@NonNull
	private final Function<Double, Double> derivate;
	
	@Override
	public final Double apply( final Double x ) {
		return this.function.apply( x );
	}
	
	@Override
	public final Function<Double, Double> derivate() {
		return this.derivate;
	}
	
}
