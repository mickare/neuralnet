package de.mickare.neuralnet;

import org.jblas.DoubleMatrix;

public interface GradientErrorSupplier {
	
	DoubleMatrix getGradientError( NeuralComputation computation );
	
	DoubleMatrix calculateHiddenGradientError( NeuralComputation computation );
	
}
