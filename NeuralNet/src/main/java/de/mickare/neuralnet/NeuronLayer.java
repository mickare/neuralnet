package de.mickare.neuralnet;

import java.util.List;
import java.util.concurrent.ExecutionException;

import lombok.Getter;

import org.jblas.DoubleMatrix;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import de.mickare.neuralnet.function.NeuronFunction;
import de.mickare.neuralnet.training.LayerTrainingSettings;
import de.mickare.neuralnet.util.concurrent.CloseableLock;
import de.mickare.neuralnet.util.concurrent.CloseableReentrantReadWriteLock;

public class NeuronLayer implements Comparable<NeuronLayer>, ResultSupplier, GradientErrorSupplier {
	
	public static final NeuronFunction DEFAULT_FUNCTION = NeuronFunction.TANH;
	
	@Getter
	private final int rows, columns;
	
	@Getter
	private final DoubleMatrix biasWeights;
	private final DoubleMatrix biasWeightDelta;
	private final CloseableReentrantReadWriteLock biasWeightsLock = new CloseableReentrantReadWriteLock( true );
	
	@Getter
	private final NeuronFunction function;
	
	@Getter
	private ImmutableSortedSet<Synapse> outgoing = ImmutableSortedSet.of();
	@Getter
	private ImmutableSortedSet<Synapse> incoming = ImmutableSortedSet.of();
	
	@Getter
	private volatile boolean closed = false;
	
	@Getter
	private final LayerTrainingSettings trainingSettings = new LayerTrainingSettings();
	
	// ************************************************************
	// Constructors
	
	public NeuronLayer( int rows, int columns ) {
		this( rows, columns, DEFAULT_FUNCTION );
	}
	
	public NeuronLayer( int rows, int columns, NeuronFunction function ) {
		Preconditions.checkArgument( rows > 0 );
		Preconditions.checkArgument( columns > 0 );
		Preconditions.checkNotNull( function );
		this.rows = rows;
		this.columns = columns;
		// this.output = new int[width][height];
		this.biasWeights = DoubleMatrix.rand( rows, columns );
		this.biasWeightDelta = DoubleMatrix.zeros( rows, columns );
		this.function = function;
	}
	
	// ************************************************************
	// Result
	
	public final ListenableFuture<DoubleMatrix> getResult( final NeuralComputation computation ) {
		Preconditions.checkNotNull( computation );
		return computation.getResult( this );
	}
	
	public final ListenableFuture<DoubleMatrix> calculateResult( final NeuralComputation computation ) {
		Preconditions.checkNotNull( computation );
		// Java Hack... to cast stream to Iterable
		final Iterable<ListenableFuture<DoubleMatrix>> iterator = this.incoming.stream()
				.map( syn -> syn.getResult( computation ) )::iterator;
		// Calculate on successfull full list
		return Futures.transform( Futures.allAsList( iterator ), this::feedForward, computation.getExecutor() );
	}
	
	private final DoubleMatrix feedForward( final List<DoubleMatrix> synapseResults ) {
		// Sum with bias
		final DoubleMatrix result;
		try ( CloseableLock cl = this.biasWeightsLock.readLock().open() ) {
			result = biasWeights.dup();
		}
		// Add all incoming results
		for ( DoubleMatrix incoming : synapseResults ) {
			result.addi( incoming );
		}
		
		// Apply function on sum
		return MatrixUtils.apply( NeuronLayer.this.function, result );
		
	}
	
	// ************************************************************
	// Gradient Error
	
	public final DoubleMatrix getGradientError( NeuralComputation computation ) {
		return computation.getGradientError( this );
	}
	
	public synchronized final DoubleMatrix calculateHiddenGradientError( NeuralComputation computation ) {
		Preconditions.checkNotNull( computation );
		
		final DoubleMatrix input = Futures.getUnchecked( this.getResult( computation ) );
		
		// Calculate the sum off all back propagated gradients * weights
		DoubleMatrix synaspesGradientSum = this.outgoing.stream().map( syn -> syn.getGradientError( computation ) )
				.reduce( DoubleMatrix.zeros( this.rows, this.columns ), ( a, b ) -> a.addi( b ) );
		
		return MatrixUtils.apply( this.getFunction().derivate(), input.dup() ).muli( synaspesGradientSum );
	}
	
	// ************************************************************
	// Back-Propagation
	
	protected synchronized void updateWeights( final NeuralComputation computation ) throws InterruptedException,
			ExecutionException {
		Preconditions.checkNotNull( computation );
		
		if ( this.incoming.isEmpty() ) {
			return;
		}
		
		final DoubleMatrix gradientErrorThisLayerNeurons = this.getGradientError( computation );
		
		for ( Synapse syn : this.incoming ) {
			syn.updateWeights( computation, gradientErrorThisLayerNeurons );
		}
		updateBiasWeights( computation, gradientErrorThisLayerNeurons );
		
	}
	
	private void updateBiasWeights( final NeuralComputation computation,
			final DoubleMatrix gradientErrorThisLayerNeurons ) {
		Preconditions.checkNotNull( computation );
		Preconditions.checkNotNull( gradientErrorThisLayerNeurons );
		
		// For a Bias the input = 1
		// So there is no input, like in SynapseFull.updateWeights(computation, gradientErrorOfNext)
		
		final double learningRate = this.getTrainingSettings().getLearningRate( computation );// eta
		final double momentum = this.getTrainingSettings().getMomentum( computation ); // alpha
		
		try ( CloseableLock cl = this.biasWeightsLock.writeLock().open() ) {
			
			this.biasWeightDelta.muli( momentum );
			this.biasWeightDelta.addi( gradientErrorThisLayerNeurons.mul( learningRate ) );
			this.biasWeights.addi( this.biasWeightDelta );
			
		}
		
	}
	
	// ************************************************************
	// Statistical Methods
	
	public int countNeurons() {
		return this.rows * this.columns;
	}
	
	public int distanceForwardTo( final NeuronLayer other ) {
		return ( other == this ) ? 0 : this.outgoing.stream()
				.mapToInt( syn -> syn.getNext().distanceForwardTo( other ) )
				.reduce( Integer.MAX_VALUE, ( o, n ) -> ( n < o ) ? n + 1 : o );
	}
	
	public int distanceBackwardsTo( final NeuronLayer other ) {
		return ( other == this ) ? 0 : this.incoming.stream()
				.mapToInt( syn -> syn.getPrev().distanceBackwardsTo( other ) )
				.reduce( Integer.MAX_VALUE, ( o, n ) -> ( n < o ) ? n + 1 : o );
	}
	
	// ************************************************************
	// Closing Neuron Layer, preventing change to network after building
	
	public synchronized void close() {
		this.closed = true;
	}
	
	// ************************************************************
	// Connect helper
	
	public SynapseFull connectFull( NeuronLayer other ) {
		return this.connect( other, SynapseFull.CONNECTOR );
	}
	
	public synchronized <S extends Synapse> S connect( NeuronLayer other, Synapse.Connector<S> connector ) {
		Preconditions.checkState( !this.closed, "Neuron Layer is closed!" );
		Preconditions.checkNotNull( other );
		Preconditions.checkNotNull( connector );
		final S synapse = connector.apply( this, other );
		Preconditions.checkNotNull( synapse );
		other.addIncoming( synapse );
		this.addOutgoing( synapse );
		return synapse;
	}
	
	private synchronized void addOutgoing( Synapse synapse ) {
		Preconditions.checkState( !this.closed, "Neuron Layer is closed!" );
		Preconditions.checkNotNull( synapse );
		Preconditions.checkArgument( synapse.getPrev() == this );
		ImmutableSortedSet.Builder<Synapse> b = ImmutableSortedSet.naturalOrder();
		b.addAll( this.outgoing );
		b.add( synapse );
		this.outgoing = b.build();
	}
	
	private synchronized void addIncoming( Synapse synapse ) {
		Preconditions.checkState( !this.closed, "Neuron Layer is closed!" );
		Preconditions.checkNotNull( synapse );
		Preconditions.checkArgument( synapse.getNext() == this );
		ImmutableSortedSet.Builder<Synapse> b = ImmutableSortedSet.naturalOrder();
		b.addAll( this.incoming );
		b.add( synapse );
		this.incoming = b.build();
	}
	
	@Override
	public int compareTo( NeuronLayer from ) {
		if ( this == from ) {
			return 0;
		}
		return Integer.compare( this.hashCode(), from.hashCode() );
	}
	
}
