package de.mickare.neuralnet;

import java.util.Arrays;

import org.jblas.DoubleMatrix;

import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import de.mickare.neuralnet.util.concurrent.CloseableLock;
import de.mickare.neuralnet.util.concurrent.CloseableReentrantReadWriteLock;

public class SynapseFull extends Synapse {
	
	public static Connector<SynapseFull> CONNECTOR = new Connector<SynapseFull>() {
		@Override
		public SynapseFull apply( NeuronLayer prev, NeuronLayer next ) {
			return new SynapseFull( prev, next );
		}
	};
	
	// Array has same dimension as output (next)
	// Value has same dimension as input (prev)
	// Weights matrix is represented as:
	// Input * weights -> weighted input
	private final DoubleMatrix[] weightsForInput;
	private final DoubleMatrix[] weightsForInputDelta;
	
	private final CloseableReentrantReadWriteLock weightsLock = new CloseableReentrantReadWriteLock( true );
	
	// ************************************************************
	// Constructors
	
	public SynapseFull( NeuronLayer prev, NeuronLayer next ) {
		super( prev, next );
		
		this.weightsForInput = new DoubleMatrix[next.getRows() * next.getColumns()];
		this.weightsForInputDelta = new DoubleMatrix[next.getRows() * next.getColumns()];
		for ( int i = 0; i < weightsForInput.length; i++ ) {
			this.weightsForInput[i] = DoubleMatrix.rand( prev.getRows(), prev.getColumns() );
			this.weightsForInputDelta[i] = DoubleMatrix.zeros( prev.getRows(), prev.getColumns() );
		}
		
	}
	
	// ************************************************************
	// Result
	
	@Override
	public ListenableFuture<DoubleMatrix> calculateResult( NeuralComputation computation ) {
		Preconditions.checkNotNull( computation );
		return Futures.transform( this.getPrev().getResult( computation ), this::feedForward );
	}
	
	private DoubleMatrix feedForward( final DoubleMatrix ingoing ) {
		final DoubleMatrix outgoing = DoubleMatrix.zeros( this.getNext().getRows(), this.getNext().getColumns() );
		try ( CloseableLock cl = this.weightsLock.readLock().open() ) {
			for ( int indexNext = 0; indexNext < outgoing.data.length; indexNext++ ) {
				final DoubleMatrix weights = this.getInputWeights( indexNext );
				outgoing.put( indexNext, Arrays.stream( weights.mul( ingoing ).data ).sum() );
			}
		}
		return outgoing;
	}
	
	// ************************************************************
	// Gradient Error
	
	public synchronized DoubleMatrix calculateHiddenGradientError( final NeuralComputation computation ) {
		Preconditions.checkNotNull( computation );
		
		final DoubleMatrix errorGradientsOfNextLayer = this.getNext().getGradientError( computation );
		
		return this.getGradientOfWeights( errorGradientsOfNextLayer );
		
		// return errorGradientsOfThisSynapse;
	}
	
	private DoubleMatrix getGradientOfWeights( DoubleMatrix errorGradientsOfNextLayer ) {
		DoubleMatrix gradientOfWeights = DoubleMatrix.zeros( this.getPrev().getRows(), this.getPrev().getColumns() );
		try ( CloseableLock cl = this.weightsLock.readLock().open() ) {
			for ( int indexNext = 0; indexNext < this.weightsForInput.length; indexNext++ ) {
				gradientOfWeights
						.addi( this.weightsForInput[indexNext].mul( errorGradientsOfNextLayer.get( indexNext ) ) );
			}
		}
		return gradientOfWeights;
	}
	
	// ************************************************************
	// Back-Propagation
	
	protected synchronized void updateWeights( final NeuralComputation computation,
			final DoubleMatrix gradientErrorOfNextLayerNeurons ) {
		Preconditions.checkNotNull( computation );
		
		gradientErrorOfNextLayerNeurons.checkLength( this.weightsForInput.length );
		
		final DoubleMatrix input = Futures.getUnchecked( this.getPrev().getResult( computation ) );
		
		final double learningRate = this.getNext().getTrainingSettings().getLearningRate( computation );// eta
		final double momentum = this.getNext().getTrainingSettings().getMomentum( computation ); // alpha
		
		try ( CloseableLock cl = this.weightsLock.writeLock().open() ) {
			
			for ( int indexNext = 0; indexNext < gradientErrorOfNextLayerNeurons.length; indexNext++ ) {
				double outputNeuronGradientError = gradientErrorOfNextLayerNeurons.get( indexNext );
				this.weightsForInputDelta[indexNext].muli( momentum );
				this.weightsForInputDelta[indexNext].addi( input.mul( outputNeuronGradientError ).mul( learningRate ) );
				
				// update weights
				this.weightsForInput[indexNext].addi( weightsForInputDelta[indexNext] );
				
			}
		}
		
	}
	
	// ************************************************************
	// Statistical
	
	@Override
	public int countNeuronSynapses() {
		return this.getPrev().getColumns() * this.getNext().getColumns() * this.getPrev().getRows()
				* this.getNext().getRows();
	}
	
	// ************************************************************
	// Weights Getter - makes your live easier
	
	/**
	 * Gets the weight for the input neuron
	 * 
	 * @param indexPrev
	 *            - index of input neuron
	 * @return matrix of dimension of output
	 */
	public final DoubleMatrix getOutputWeights( final int indexPrev ) {
		final DoubleMatrix result = new DoubleMatrix( this.getNext().getRows(), this.getNext().getColumns() );
		for ( int indexNext = 0; indexNext < this.weightsForInput.length; indexNext++ ) {
			result.put( indexNext, this.getInputWeights( indexNext ).get( indexPrev ) );
		}
		return result;
	}
	
	/**
	 * Gets the weight index for the output neuron
	 * 
	 * @param rowNext
	 * @param columnNext
	 * @return index for weights array
	 */
	public final int indexInputWeights( final int rowNext, final int columnNext ) {
		return rowNext + this.getNext().getRows() * columnNext;
	}
	
	/**
	 * Gets the weights for the output neuron
	 * 
	 * @param indexNext
	 * @return matrix of dimension of input
	 */
	public final DoubleMatrix getInputWeights( final int indexNext ) {
		return this.weightsForInput[indexNext];
	}
	
	/**
	 * Gets the weights for the output neuron
	 * 
	 * @param rowNext
	 * @param columnNext
	 * @return matrix of dimension of input
	 */
	public final DoubleMatrix getInputWeights( final int rowNext, final int columnNext ) {
		return this.getInputWeights( indexInputWeights( rowNext, columnNext ) );
	}
	
}
