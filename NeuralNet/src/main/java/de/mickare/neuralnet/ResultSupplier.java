package de.mickare.neuralnet;

import org.jblas.DoubleMatrix;

import com.google.common.util.concurrent.ListenableFuture;

public interface ResultSupplier {
	
	ListenableFuture<DoubleMatrix> getResult( NeuralComputation computation );
	
	ListenableFuture<DoubleMatrix> calculateResult( NeuralComputation computation );
	
}
