# README #

My first try to create a neural network in Java.

### Description ###

* I started with the video by David Miller (https://vimeo.com/19569529) and extended the network to asynchronous forward feeding.
* Version 1.0.0

### ToDo ###

* The training doesn't work asynchronously.

### Other stuff ###

* Copyright 2015 by mickare, All rights reserved