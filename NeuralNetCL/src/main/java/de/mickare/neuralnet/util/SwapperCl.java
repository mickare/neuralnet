package de.mickare.neuralnet.util;

import com.amd.aparapi.Range;
import com.amd.aparapi.opencl.OpenCL;

public interface SwapperCl extends OpenCL<SwapperCl> {
	@Kernel( "{\n"//
			+ "  const size_t id = get_global_id(0);\n"//
			+ "  float temp=lhs[id];" + "  lhs[id] = rhs[id];\n"//
			+ "  rhs[id] = temp;\n"//
			+ "}\n" )
	
	public SwapperCl swap( //
			Range _range,//
			@GlobalReadWrite( "lhs" ) float[] lhs,//
			@GlobalReadWrite( "rhs" ) float[] rhs );
}
