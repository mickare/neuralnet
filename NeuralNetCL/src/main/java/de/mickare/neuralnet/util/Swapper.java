package de.mickare.neuralnet.util;

import com.amd.aparapi.Range;

public interface Swapper<SELF> {
	
	public SELF swap( Range _range, float[] lhs, float[] rhs );
	
}
