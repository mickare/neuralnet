package de.mickare.neuralnet.tutorial;

import java.util.Random;

import com.amd.aparapi.Kernel;

public class AparapiMatMul extends Kernel {
	
	double matA[];
	double matB[];
	double matC[];
	double C[];
	
	int rows;
	int cols1;
	int cols2;
	
	@Override
	public void run() {
		int i = getGlobalId() / rows;
		int j = getGlobalId() % rows;
		double value = 0;
		for ( int k = 0; k < cols1; k++ ) {
			value += matA[k + i * cols1] * matB[k * cols2 + j];
		}
		matC[i * cols1 + j] = value;
	}
	
	public AparapiMatMul( int r, int c1, int c2 ) {
		
		rows = r;
		cols1 = c1;
		cols2 = c2;
		
		matA = new double[r * c1];
		matB = new double[c1 * c2];
		matC = new double[r * c2];
		C = new double[r * c2];
		// matC should be initialized with zeros
		for ( int i = 0; i < r; i++ ) {
			for ( int j = 0; j < c1; j++ ) {
				matC[i * c1 + j] = 0;
				
			}
		}
		
		// Here matrix A is initialized with random numbers
		
		for ( int i = 0; i < r; i++ ) {
			for ( int j = 0; j < c1; j++ ) {
				matA[i * c1 + j] = new Random().nextDouble();
			}
		}
		
		// Here matrix B is initialized with random numbers
		
		for ( int i = 0; i < r; i++ ) {
			for ( int j = 0; j < c1; j++ ) {
				matB[i * c2 + j] = new Random().nextDouble();
			}
		}
		
	}
	
	public void printResults() {
		for ( int i = 0; i < rows; i++ ) {
			for ( int j = 0; j < cols2; j++ ) {
				System.out.print( matC[i * cols2 + j] + "    " );
			}
		}
	}
	
	public void normalMatMulCalc() {
		for ( int i = 0; i < rows; i++ ) {
			for ( int j = 0; j < cols2; j++ ) {
				double sum = 0;
				for ( int k = 0; k < cols1; k++ ) {
					sum += matA[i * cols1 + k] * matB[k * rows + j];
				}
				C[i * cols2 + j] = sum;
			}
			
		}
	}
	
	public void compareResults() {
		boolean equal = true;
		for ( int i = 0; i < rows * cols2; i++ ) {
			if ( matC[i] != C[i] ) {
				equal = false;
				break;
			}
		}
		if ( !equal )
			System.out.println( "Results are not equal" );
		else
			System.out.println( "Results are equal.. Tested thoroughly!!!" );
	}
	
}
