package de.mickare.neuralnet;

import java.io.IOException;

import org.jblas.DoubleMatrix;

import com.amd.aparapi.Range;
import com.amd.aparapi.device.Device;
import com.amd.aparapi.device.OpenCLDevice;

import de.mickare.neuralnet.opencl.SynapseKernelWrapper;
import de.mickare.neuralnet.opencl.old.SingleSynapseCalc;

public class Main {
	
	public static final void main( final String[] args ) throws IOException {
		
		Device device = Device.firstGPU();
		
		if ( device instanceof OpenCLDevice ) {
			
			OpenCLDevice oDevice = ( OpenCLDevice ) device;
			
			final int inLength = 2;
			final int outLength = 4;
			
			SynapseKernelWrapper syn = new SynapseKernelWrapper( oDevice, outLength, inLength );
			
			DoubleMatrix input = DoubleMatrix.ones( inLength, 1 );
			DoubleMatrix weights = DoubleMatrix.rand( outLength, inLength );
			
			syn.calc( weights, input ).print();
			
			DoubleMatrix seq = weights.mmul( input );
			for ( int i = 0; i < seq.length; i++ ) {
				seq.data[i] = Math.tanh( seq.data[i] );
			}
			seq.print();
			
		}
		
	}
	
	private static void test( OpenCLDevice oDevice ) {
		
		System.out.println( "A" );
		SingleSynapseCalc calc = oDevice.bind( SingleSynapseCalc.class );
		
		DoubleMatrix in = DoubleMatrix.ones( 2, 1 );
		final double[] out = new double[4];
		DoubleMatrix weights = DoubleMatrix.rand( out.length, in.length );
		
		Range range = oDevice.createRange2D( weights.columns, weights.rows, oDevice.getMaxWorkGroupSize(), 1 );
		final double[] local_sums = new double[oDevice.getMaxWorkGroupSize()];
		// Ensure for enough space in a line
		int global_line_length = weights.columns % oDevice.getMaxWorkGroupSize() == 0 ? weights.columns
				/ oDevice.getMaxWorkGroupSize() : weights.columns / oDevice.getMaxWorkGroupSize() + 1;
		final double[] global_sums = new double[weights.rows * global_line_length];
		
		calc.calc( range, local_sums, global_sums, weights.data, in.data, in.length, out, out.length );
		
		new DoubleMatrix( out.length, 1, out ).print();
		
		DoubleMatrix seq = weights.mmul( in );
		for ( int i = 0; i < seq.length; i++ ) {
			seq.data[i] = Math.tanh( seq.data[i] );
		}
		seq.print();
	}
	
}
