package de.mickare.neuralnet.opencl.old;

import com.amd.aparapi.Range;
import com.amd.aparapi.opencl.OpenCL;

@OpenCL.Resource( "de/mickare/neuralnet/opencl/SingleSynapseCalc.cl" )
public interface SingleSynapseCalc extends OpenCL<SingleSynapseCalc> {
	
	public SingleSynapseCalc calc( //
			Range _range,//
			@Local( "local_sums" ) double[] local_sums,//
			@GlobalReadWrite( "global_sums" ) double[] global_sums, @GlobalReadOnly( "weights" ) double[] weights,//
			@GlobalReadOnly( "in" ) double[] in,//
			@Constant( "inLength" ) int inLength,//
			@GlobalWriteOnly( "out" ) double[] out,//
			@Constant( "outLength" ) int outLength );
	
}
