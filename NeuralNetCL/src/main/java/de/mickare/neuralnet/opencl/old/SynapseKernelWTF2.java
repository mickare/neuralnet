package de.mickare.neuralnet.opencl.old;

import org.jblas.DoubleMatrix;

import com.amd.aparapi.Kernel;
import com.amd.aparapi.Range;
import com.amd.aparapi.device.OpenCLDevice;
import com.google.common.base.Preconditions;

public class SynapseKernelWTF2 {
	
	private final Kernel kernel;
	private final Range range;
	
	private final double[] weights;
	private final double[] input;
	private final double[] output;
	
	private final int rows, columns;
	
	public SynapseKernelWTF2( final OpenCLDevice device, final int rows, final int columns ) {
		Preconditions.checkNotNull( device );
		Preconditions.checkArgument( rows > 0 );
		Preconditions.checkArgument( columns > 0 );
		
		this.rows = rows;
		this.columns = columns;
		
		this.weights = new double[rows * columns];
		this.input = new double[columns];
		this.output = new double[rows];
		
		this.range = device.createRange2D( columns, rows, device.getMaxWorkGroupSize(), 1 );
		
		// Ensure for enough space in a line
		final int global_line_length = columns % device.getMaxWorkGroupSize() == 0 ? columns
				/ device.getMaxWorkGroupSize() : columns / device.getMaxWorkGroupSize() + 1;
		
		final double[] global_sums = new double[rows * global_line_length];
		
		this.kernel = new Kernel() {
			@Local
			final double[] local_sums = new double[range.getLocalSize( 0 ) * range.getLocalSize( 1 )];
			
			@Override
			public void run() {
				
				final int row = getGroupId( 1 );
				// final int rowGroupId = getGroupId( 1 );
				final int column = getGlobalId( 0 );
				final int columnGroupId = getGroupId( 0 );
				
				// final int groupID_column = getGroupId( 0 );
				final int localID = getLocalId( 0 );
				final int localSize = getLocalSize( 0 );
				
				final int weightsOffset = row + column;
				
				if ( column < columns ) {
					local_sums[localID] = weights[weightsOffset] * input[column];
				} else {
					local_sums[localID] = 0;
				}
				
				// rmFactor = reduction module factor
				final int rmFactorMax = localSize / 2;
				for ( int rmFactor = 1; rmFactor < rmFactorMax; rmFactor *= 2 ) {
					this.localBarrier();
					if ( localID % ( rmFactor * 2 ) == 0 ) {
						local_sums[localID] += local_sums[localID + rmFactor];
					}
				}
				// local_sums[0] is the result of the reduction
				
				final int wgroup_columns = getNumGroups( 0 );
				final int globalSumOffset = wgroup_columns * row;
				
				if ( localID == 0 ) {
					global_sums[globalSumOffset + columnGroupId] = local_sums[0];
				}
				
				if ( columnGroupId == 0 ) {
					// Iteratote only this one over all small workgroup sums
					// Later for huge workgroup sizes, there can be also a reduction added here
					double groupSum = local_sums[0];
					// Starting at next group, since we already have the sum of this element!
					for ( int g = 1; g < wgroup_columns; g++ ) {
						groupSum += global_sums[globalSumOffset + g];
					}
					output[row] = Math.tanh( groupSum );
				}
				
			}
		};
		this.kernel.setExplicit( true );
		
	}
	
	public DoubleMatrix calc( DoubleMatrix weights, DoubleMatrix input ) {
		weights.checkColumns( this.columns );
		weights.checkRows( this.rows );
		input.checkColumns( 1 );
		input.checkRows( this.columns );
		return new DoubleMatrix( this.rows, 1, this.calc( weights.data, input.data ) );
	}
	
	public synchronized double[] calc( double[] weights, double[] input ) {
		Preconditions.checkArgument( weights.length == this.weights.length );
		Preconditions.checkArgument( input.length == this.input.length );
		System.arraycopy( weights, 0, this.weights, 0, weights.length );
		System.arraycopy( input, 0, this.input, 0, input.length );
		this.kernel.put( this.weights );
		this.kernel.put( this.input );
		this.kernel.execute( this.range ).get( this.output );
		final double[] resultOutput = new double[this.output.length];
		System.arraycopy( this.output, 0, resultOutput, 0, resultOutput.length );
		return resultOutput;
	}
	
}
