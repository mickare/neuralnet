package de.mickare.neuralnet.opencl;

import lombok.Getter;

import org.jblas.DoubleMatrix;

import com.amd.aparapi.Range;
import com.amd.aparapi.device.OpenCLDevice;
import com.google.common.base.Preconditions;

public class SynapseKernelWrapper {
	
	@Getter
	private final OpenCLDevice device;
	@Getter
	private final SynapseKernel kernel;
	
	@Getter
	private final int columns, rows;
	
	private final double[] local_sums, global_sums;
	
	public SynapseKernelWrapper( OpenCLDevice device, final int outLength, final int inLength ) {
		Preconditions.checkArgument( inLength > 0 );
		Preconditions.checkArgument( outLength > 0 );
		this.columns = inLength;
		this.rows = outLength;
		this.kernel = device.bind( SynapseKernel.class );
		this.device = device;
		final Range range = device.createRange2D( inLength, outLength );
		this.local_sums = new double[range.getLocalSize( 0 ) * range.getLocalSize( 1 )];
		this.global_sums = new double[range.getNumGroups( 0 ) * range.getNumGroups( 1 )];
		
	}
	
	public DoubleMatrix calc( DoubleMatrix weights, DoubleMatrix input ) {
		weights.checkColumns( this.columns );
		weights.checkRows( this.rows );
		input.checkColumns( 1 );
		input.checkRows( this.columns );
		return new DoubleMatrix( this.rows, 1, this.calc( weights.data, input.data ) );
	}
	
	public synchronized double[] calc( double[] weights, double[] input ) {
		Preconditions.checkArgument( weights.length == this.columns * this.rows );
		Preconditions.checkArgument( input.length == this.columns );
		
		final double[] output = new double[this.rows];
		this.kernel
				.calc( this.device.createRange2D( columns, rows ), this.columns, local_sums, global_sums, weights, input, output );
		return output;
	}
}
