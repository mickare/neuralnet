#pragma OPENCL EXTENSION cl_khr_fp64 : enable

__kernel void calc(
   const int columns, 
   __local double *local_sums, 
   __global double *global_sums, 
   __global const double *weights, 
   __global const double *input, 
   __global double *output
){
      int row = get_group_id(1);
      int column = get_global_id(0);
      int columnGroupId = get_group_id(0);
      int localID = get_local_id(0);
      int localSize = get_local_size(0);
      int weightsOffset = row + column;
      if (column < columns){
         local_sums[localID]  = weights[weightsOffset] * input[column];
      } else {
         local_sums[localID]  = 0.0;
      }
      int rmFactorMax = localSize / 2;
      for (int rmFactor = 1; rmFactor<rmFactorMax; rmFactor = rmFactor * 2){
         barrier(CLK_LOCAL_MEM_FENCE);
         if ((localID % (rmFactor * 2))==0){
            local_sums[localID]  = local_sums[localID] + local_sums[(localID + rmFactor)];
         }
      }
      int wgroup_columns = get_num_groups(0);
      int globalSumOffset = wgroup_columns * row;
      if (localID==0){
         global_sums[globalSumOffset + columnGroupId]  = local_sums[0];
      }
      if (columnGroupId==0){
         double groupSum = local_sums[0];
         for (int g = 1; g<wgroup_columns; g++){
            groupSum = groupSum + global_sums[(globalSumOffset + g)];
         }
         output[row]  = groupSum;
      }
      return;
}