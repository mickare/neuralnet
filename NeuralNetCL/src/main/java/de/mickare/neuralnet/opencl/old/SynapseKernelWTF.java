package de.mickare.neuralnet.opencl.old;

import org.jblas.DoubleMatrix;

import com.amd.aparapi.Kernel;
import com.amd.aparapi.Range;
import com.amd.aparapi.device.Device;
import com.google.common.base.Preconditions;

public class SynapseKernelWTF extends Kernel {
	
	private final int rows;
	private final int columns;
	
	private final double[] weights;
	private final double[] input;
	private final double[] output;
	
	private final double[] global_sums;
	
	final double[] local_sums_$local$;
	
	public SynapseKernelWTF( final Device device, final int rows, final int columns ) {
		Preconditions.checkArgument( rows > 0 );
		Preconditions.checkArgument( columns > 0 );
		final Range range = device.createRange2D( columns, rows );
		this.columns = range.getGlobalSize( 0 );
		this.rows = range.getGlobalSize( 1 );
		this.input = new double[columns];
		this.output = new double[rows];
		this.weights = new double[columns * rows];
		this.local_sums_$local$ = new double[range.getLocalSize( 0 ) * range.getLocalSize( 1 )];
		this.global_sums = new double[range.getNumGroups( 0 ) * range.getNumGroups( 1 )];
		this.setExplicit( true );
	}
	
	@Override
	public void run() {
		
		final int row = getGroupId( 1 );
		// final int rowGroupId = getGroupId( 1 );
		final int column = getGlobalId( 0 );
		final int columnGroupId = getGroupId( 0 );
		
		// final int groupID_column = getGroupId( 0 );
		final int localID = getLocalId( 0 );
		final int localSize = getLocalSize( 0 );
		
		final int weightsOffset = row + column;
		
		if ( column < columns ) {
			local_sums_$local$[localID] = weights[weightsOffset] * input[column];
		} else {
			local_sums_$local$[localID] = 0;
		}
		
		// rmFactor = reduction module factor
		final int rmFactorMax = localSize / 2;
		for ( int rmFactor = 1; rmFactor < rmFactorMax; rmFactor *= 2 ) {
			this.localBarrier();
			if ( localID % ( rmFactor * 2 ) == 0 ) {
				local_sums_$local$[localID] += local_sums_$local$[localID + rmFactor];
			}
		}
		// local_sums[0] is the result of the reduction
		
		final int wgroup_columns = getNumGroups( 0 );
		final int globalSumOffset = wgroup_columns * row;
		
		if ( localID == 0 ) {
			global_sums[globalSumOffset + columnGroupId] = local_sums_$local$[0];
		}
		
		if ( columnGroupId == 0 ) {
			// Iteratote only this one over all small workgroup sums
			// Later for huge workgroup sizes, there can be also a reduction added here
			double groupSum = local_sums_$local$[0];
			// Starting at next group, since we already have the sum of this element!
			for ( int g = 1; g < wgroup_columns; g++ ) {
				groupSum += global_sums[globalSumOffset + g];
			}
			output[row] = groupSum;
		}
		
	}
	
	public DoubleMatrix calc( final Device device, DoubleMatrix weights, DoubleMatrix input ) {
		weights.checkColumns( this.columns );
		weights.checkRows( this.rows );
		input.checkColumns( 1 );
		input.checkRows( this.columns );
		return new DoubleMatrix( this.rows, 1, this.calc( device, weights.data, input.data ) );
	}
	
	public synchronized double[] calc( final Device device, double[] weights, double[] input ) {
		Preconditions.checkArgument( weights.length == this.weights.length );
		Preconditions.checkArgument( input.length == this.input.length );
		System.arraycopy( weights, 0, this.weights, 0, weights.length );
		System.arraycopy( input, 0, this.input, 0, input.length );
		// final Range range = device.createRange2D( this.columns, this.rows );
		
		this.put( this.weights );
		this.put( this.input );
		this.execute( device.createRange2D( columns, rows ) ).get( this.output );
		final double[] resultOutput = new double[this.output.length];
		System.arraycopy( this.output, 0, resultOutput, 0, resultOutput.length );
		return resultOutput;
	}
	
}
