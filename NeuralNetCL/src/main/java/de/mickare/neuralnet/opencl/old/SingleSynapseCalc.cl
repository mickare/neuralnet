#pragma OPENCL EXTENSION cl_khr_fp64 : enable

// Matrix multiplication
// (m*n)(n*1) = (m*1)


__kernel void calc(
  __local double *local_sums,
  __global double *global_sums,
	__global const double *weights,
  const int width, // width = input length
  const int height, // height = output length
	__global const double *input,
	__global double *output
){
  // Global ID 0 = column = on width axis
  // Global ID 1 = row = on height axis
  const int wgroup_columnID = get_group_id(0);
  const int wgroup_rowID = get_group_id(1);
	const int localID = get_local_id(0);
  const int localSize = get_local_size(0);
  
  // height not needed here
  const int workerWeightsOffsetInLine = localSize * wgroup_columnID + localID;
  const int workerWeightsOffset = width * wgroup_rowID + workerWeightsOffsetInLine;
    
  if(workerWeightsOffsetInLine < width) {
    local_sums[localID] = weights[workerWeightsOffset] * input[workerWeightsOffsetInLine];
  } else {
    // Don't use worker, if it is out range!
    local_sums[localID] = 0;
  }
  
  // Sum reduction
  // The local worker that has the module takes 
  // rmFactor = reduction module factor
  const int rmFactorMax = localSize / 2;
  for(int rmFactor = 1; rmFactor < rmFactorMax; rmFactor *= 2) {
    barrier(CLK_LOCAL_MEM_FENCE); // Complicated for loop to prevent deadlocks.
    if(localID % (rmFactor * 2) == 0) {
      local_sums[localID] += local_sums[localID + rmFactor];
    }
  }
  // local_sums[0] is the result of the reduction
  
  
  // Write the sum of the workgroups to a global matrix to
  // do a sum on all workgroups of a single row.
  const int wgroup_columns = get_num_groups(0);
  //const int wgroup_rows = get_num_groups(0);
  
  const int wgroup_offset = wgroup_columns * wgroup_rowID;
  
  if(localID == 0) {
    global_sums[wgroup_offset + wgroup_columnID] = local_sums[0];
  }
    
  if(wgroup_columnID == 0) {
    // Iteratote only this one over all small workgroup sums
    // Later for huge workgroup sizes, there can be also a reduction added here
    double group_sum = local_sums[0];
    // Starting at next group, since we already have the sum of this element!
    for(int g = 1; g < wgroup_columns; g++) {
      group_sum += global_sums[wgroup_offset + g];
    }
    output[wgroup_rowID] = group_sum;
  }
  
}