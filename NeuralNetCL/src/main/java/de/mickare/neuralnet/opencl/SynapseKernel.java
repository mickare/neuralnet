package de.mickare.neuralnet.opencl;

import com.amd.aparapi.Range;
import com.amd.aparapi.opencl.OpenCL;

@OpenCL.Resource( "de/mickare/neuralnet/opencl/SynapseKernel.cl" )
public interface SynapseKernel extends OpenCL<SynapseKernel> {
	
	public SynapseKernel calc( //
			Range _range,//
			@Constant( "columns" ) int columns,//
			@Local( "local_sums" ) double[] local_sums,//
			@GlobalReadWrite( "global_sums" ) double[] global_sums,//
			@GlobalReadOnly( "weights" ) double[] weights,//
			@GlobalReadOnly( "input" ) double[] input,//
			@GlobalWriteOnly( "output" ) double[] output );
	
}
